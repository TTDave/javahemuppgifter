import java.util.Scanner;

public class Mom3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Mata in din längd: ");
		double height = input.nextDouble();
		
		System.out.println("Mata in din vikt: ");
		double weight = input.nextDouble();
		
		double BMI = weight / (height/100*height/100);
		System.out.println("Ditt BMI är: " + BMI);
		
		if ( BMI < 18.5 ) {
			System.out.println("Undervikt");
		}
		else if ( BMI >= 18.5 && BMI < 24.9 ) {
			System.out.println("Normalvikt");
		}
		else if ( BMI >= 25 && BMI < 29.9 ) {
			System.out.println("Övervikt");
		}
		else if ( BMI >= 30 ) {
			System.out.println("Fetma");
		}
	}

}
