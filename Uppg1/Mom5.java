import java.util.Scanner;

public class Mom5 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner( System.in );
		
		System.out.println("Mata in ett årtal och kolla om det är ett skottår: ");
		int year = in.nextInt();
		
		boolean x = ( year % 4 ) == 0;
		boolean y = ( year % 100 ) == 0;
		boolean z = (( year % 100 == 0 ) && ( year % 400  == 0));
		
		if ( x && ( y || z ) ) 
		{
			System.out.println( year + " är ett skottår." );
		}
		else
		{
			System.out.println( year + " är inte ett skottår." );
		}
	}

}
