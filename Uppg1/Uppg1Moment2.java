import java.util.Scanner; 

public class Uppg1Moment2 {

	public static void main(String[] args) {
		//Fr�gar i konsolen efter cirkelns diameter.
		System.out.println("Vad �r diametern p� cirkeln? (inga enheter!)");
		
		//Tar input av anv�ndaren och tilldelar det till en string 'diameterString' med hj�lp av en scanner.
		Scanner scan = new Scanner(System.in);
		String diameterString = scan.nextLine();
		
		//Skapar en double 'diameter' som tolkar stringen.
		Double diameter = Double.parseDouble(diameterString);
		
		//Skapar en double 'radius' som �r halva v�rdet av diamtetern.
		Double radian = diameter/2;
		
		//Skapar en double 'area' som �r PI * radius^2.
		Double area = Math.PI*Math.pow(radian, 2);
		
		//Skapar en string 'areaString' f�r att kunna skriva ut arean.
		String areaString = area.toString();
		
		//Skriver ut en resultatet.
		System.out.println("Arean p� en cirkel med diametern " + diameterString + " �r " + areaString + ".");
	}

}