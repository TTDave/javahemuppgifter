import javax.swing.JOptionPane;

public class Uppg1Moment6 {

	//Ber om urs�kt i f�rv�g f�r denna gr�sliga kod!
	public static void main(String[] args) {
		
		//Fr�gar efter anv�ndaren efter ett �rtal och tilldelar v�rdet till stringen 'yearString'.
		String yearString = JOptionPane.showInputDialog("Mata in ett �rtal! (1752 eller tidigare fungerar inte!)");
		
		//Skapar integern 'yearInt' fr�n stringen 'yearString'.
		int yearInt = Integer.parseInt(yearString);
		
		//Fr�gar efter anv�ndaren efter ett �rtal och tilldelar v�rdet till stringen 'yearString'.
				String monthString = JOptionPane.showInputDialog("Mata in ett m�nad (heltal!)");
				
				//Skapar integern 'monthInt' fr�n stringen 'monthString'.
				int monthInt = Integer.parseInt(monthString);
				
				String month = "";
				String daysString = "";
				
		if (yearInt > 1752)
		{
			/////////////////////////////////////////
			//////KOLLAR OM �RET �R ETT SKOTT�R//////
			/////////////////////////////////////////
			
			//Skapar en boolean och l�gger den by default till false
			boolean leapYear = false;
			
			//Om �ret �r delbart med 4
			if(yearInt % 4 == 0)
			{
			//Om �ret �r delbart med 100
			if(yearInt % 100 == 0)
			{
			//Om �ret �r delbart med 400 (�rhundradesiffra delbart med 4)
			if(yearInt % 400 == 0)
			{
			leapYear = true;
			}
			
			//Om �ret �r delbart med 4, 100 och inte 400
			else 
			{
			leapYear = false;
			}
			
			}
			//Om �ret �r delbart med 4 och inte 100
			else 
			{
			
			leapYear = true;
			}
			}
			//Om �ret inte delbart med 4
			else 
			{
			leapYear = false;
			}
			
			//////////////////////////////////////////////
			//////KOLLAR HUR M�NGA DAGAR M�NADEN HAR//////
			//////////////////////////////////////////////
			
			//Om det �r Januari
			if(monthInt == 1)
			{
			month = "Januari";
			daysString = "31";
			}
			
			//Om det �r Februari
			if(monthInt == 2)
			{
			month = "Februari";
			//Kollar om det �r skott�r
			if(leapYear == true)
			{
			daysString = "29";
			}
			else
			{
			daysString = "28";
			}
			}
			
			//Om det �r Mars
			if(monthInt == 3)
			{
			month = "Mars";
			daysString = "31";
			}
			
			//Om det �r April
			if(monthInt == 4)
			{
			month = "April";
			daysString = "30";
			}
			
			//Om det �r Maj
			if(monthInt == 5)
			{
			month = "Maj";
			daysString = "31";
			}
			
			//Om det �r Juni
			if(monthInt == 6)
			{
			month = "Juni";
			daysString = "30";
			}
			
			//Om det �r Juli 
			if(monthInt == 7)
			{
			month = "Juli";
			daysString = "31";
			}
			
			//Om det �r Augusti 
			if(monthInt == 8)
			{
			month = "Augusti";
			daysString = "31";
			}
			
			//Om det �r September 
			if(monthInt == 9)
			{
			month = "September";
			daysString = "30";
			}
			
			//Om det �r Oktober 
			if(monthInt == 10)
			{
			month = "Oktober";
			daysString = "31";
			}
			
			//Om det �r November 
			if(monthInt == 11)
			{
			month = "November";
			daysString = "30";
			}
			
			//Om det �r December 
			if(monthInt == 12)
			{
			month = "December";
			daysString = "31";
			}
			
			JOptionPane.showMessageDialog(null, month + " �r " + yearString + " har " + daysString + " dagar!");
		}
		else 
		{
			JOptionPane.showMessageDialog(null, "Kan inte kolla �r tidigare �n eller lika med �r 1752!");
		}
		
		
		
		
		
	}
	

}
