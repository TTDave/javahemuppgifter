import java.util.Scanner;

public class Mom6 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int Nummer_Dagar_I_Månad = 0;
		String månadsNamn = "Mata in ett korrekt värde";
		
		System.out.println("Mata in en månad (1-12): ");
		int månad = input.nextInt();
		
		System.out.println("Mata in ett årtal: ");
		int år = input.nextInt();
		
		switch (månad) {
		case 1:
            månadsNamn = "January";
            Nummer_Dagar_I_Månad = 31;
            break;
        case 2:
            månadsNamn = "Februari";
            if ((år % 400 == 0) || ((år % 4 == 0) && (år % 100 != 0))) {
            	Nummer_Dagar_I_Månad = 29;
            } else {
            	Nummer_Dagar_I_Månad = 28;
            }
            break;
        case 3:
            månadsNamn = "Mars";
            Nummer_Dagar_I_Månad = 31;
            break;
        case 4:
            månadsNamn = "April";
            Nummer_Dagar_I_Månad = 30;
            break;
        case 5:
            månadsNamn = "Maj";
            Nummer_Dagar_I_Månad = 31;
            break;
        case 6:
            månadsNamn = "Juni";
            Nummer_Dagar_I_Månad = 30;
            break;
        case 7:
            månadsNamn = "Juli";
            Nummer_Dagar_I_Månad = 31;
            break;
        case 8:
            månadsNamn = "Augusti";
            Nummer_Dagar_I_Månad = 31;
            break;
        case 9:
            månadsNamn = "September";
            Nummer_Dagar_I_Månad = 30;
            break;
        case 10:
            månadsNamn = "Oktober";
            Nummer_Dagar_I_Månad = 31;
            break;
        case 11:
            månadsNamn = "November";
            Nummer_Dagar_I_Månad = 30;
            break;
        case 12:
            månadsNamn = "December";
            Nummer_Dagar_I_Månad = 31;
		}
		System.out.print(månadsNamn + " " + år + " har " + Nummer_Dagar_I_Månad + " dagar\n");

	}

}
