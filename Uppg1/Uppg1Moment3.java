import java.util.Scanner; 

public class Uppg1Moment3 {

	public static void main(String[] args) {
		//Fr�gar i konsolen efter anv�ndarens h�jd.
		System.out.println("Vad �r din h�jd? (heltal, cm!)");
		
		//Tar input av anv�ndaren och tilldelar det till en string 'heightString' med hj�lp av en scanner.
		Scanner scan = new Scanner(System.in);
		String heightString = scan.nextLine();
		

		//Fr�gar i konsolen efter anv�ndarens vikt.
		System.out.println("Vad �r din vikt? (heltal, kg!)");
		
		//Tar input av anv�ndaren och tilldelar det till en string 'heightString' med hj�lp av tidigare skapade scanner.
		String weightString = scan.nextLine();
		
		//Skapar integerna 'height' och 'weight' som tolkar respektive string.
		Integer height = Integer.parseInt(heightString);
		Integer weight = Integer.parseInt(weightString);

		//Skapar doubles  'heightDouble' och 'weightDouble' f�r att kunna r�kna en BMI double. Skapar en ny double 'heightDoubleMeter* som �r l�ngden i meter.
		Double heightDouble = new Double(height);
		Double weightDouble = new Double(weight);
		Double heightDoubleMeter = heightDouble/100;
		
		//Skapar double 'BMI' som r�knar ut vikten med formeln kg/(m*m). 
		Double BMI = weightDouble/(heightDoubleMeter*heightDoubleMeter);
		
		//Skapar ytterligare en double som tar v�rdet av BMI och avrundar till en decimal.
		Double BMIRounded = Math.round(BMI * 10) / 10.0;
		
		////Skapar en string 'BMIString' f�r att kunna skriva ut resultatet.
		String BMIString = BMIRounded.toString();
		System.out.println(BMIString);
		
		
		
	}

}
