import java.util.Scanner; 

public class Uppg1Moment1 {

	public static void main(String[] args) {
		
		//Fr�gar i konsolen efter anv�ndarens namn.
		System.out.println("Vad �r ditt namn?");
		
		//Tar input av anv�ndaren och tilldelar det till en string med hj�lp av en scanner.
		Scanner scan = new Scanner(System.in);
		String name = scan.nextLine();
		
		//Skriver ut en h�lsningsfras.
		System.out.println("Hej " + name + "!");
	}

}
