import java.util.Scanner;
import javax.swing.*;

public class Uppg1Moment9 {

	public static void main(String[] args) {

		//Fr�gar i konsolen efter anv�ndarens namn.
		System.out.println("Ange ett heltal:");
		
		//Tar input av anv�ndaren och tilldelar det till en string med hj�lp av en scanner.
		Scanner scan = new Scanner(System.in);
		String multiplicandString = scan.nextLine();
		scan.close();
		
		//Tilldelar input i form av en integer till ena faktorn i multiplikationen
		int multiplicand = Integer.parseInt(multiplicandString);
		
		//Skapar tv� variabler som kan h�lla resultaten i form av integer och string
		int product;
		String productString;
		
		//Skapar en array som har utrymme f�r alla multiplikationer fr�n 1 till 10
		String[] table = new String[10];
		
		//En for loop d�r vi definierar andra faktorn och anv�nder den f�r att g� igenom multiplikationstabellen
		for(int multiplier = 1; multiplier < 11; multiplier++)
		{
			//r�knar och l�gger v�rdena till sina respektive variabler, skapar en string f�r andra faktorn
			product = (multiplier) * multiplicand;
			productString = Integer.toString(product);
			String multiplierString = Integer.toString(multiplier);
			
			//matar in en kombination av strings till varje index fr�n 0 till 9
			table[multiplier-1] = (multiplierString + " * " + multiplicandString + " = " + productString);
			
		}
		//av n�gon orsak dyker denna upp bakom Eclipse...
		JOptionPane.showMessageDialog(null, table);
		

	}

}
