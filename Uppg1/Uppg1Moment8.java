import javax.swing.JOptionPane;

public class Uppg1Moment8 {

	public static void main(String[] args) {
		
		String inputString = JOptionPane.showInputDialog("Mata in ett ett decimaltal (anv�nd punkt och inte komma!");
		
		Double inputDouble = Double.parseDouble(inputString);
		
		boolean prime = true;
		

		
		
		
		//Kollar om talet �r heltal
		if(inputDouble % 1.0 == 0)
		{
			
			//Kollar om talet �r ett primtal
			for(double i = 2.0; i < inputDouble/2.0; i++)
			{
				if(inputDouble % i == 0)
				{
					prime = false;
				}
			}
			
			//Om talet �r delbart med tv� och ett primtal
			if((inputDouble % 2.0 == 0) && (prime==true))
			{
				JOptionPane.showMessageDialog(null, inputString + " �r ett j�mnt heltal och ett primtal!");
			}
			else 
				//Om talet �r delbart med tv� men inte ett primtal
				if((inputDouble % 2.0 == 0) && (prime==false))
				{
					JOptionPane.showMessageDialog(null, inputString + " �r ett j�mnt heltal, men inte ett primtal!");
				}
				else 
					//Om talet inte �r delbart med tv�, men �r ett primtal
					if(prime == true)
					{
					JOptionPane.showMessageDialog(null, inputString + " �r ett udda heltal och ett primtal!");
					}
					//Om talet varken �r delbart med tv� eller primtal
					else
					{
						JOptionPane.showMessageDialog(null, inputString + " �r ett udda heltal, men inte ett primtal!");
					}
		}
		else
		{
			JOptionPane.showMessageDialog(null, inputString + " �r inte ett heltal!");
		}
		

	}

}
