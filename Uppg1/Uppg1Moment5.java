import javax.swing.JOptionPane;

public class Uppg1Moment5 {

	public static void main(String[] args) {
		
		//Fr�gar efter anv�ndaren efter ett �rtal och tilldelar v�rdet till stringen 'yearString'.
		String yearString = JOptionPane.showInputDialog("Mata in ett �rtal, s� kollar vi om det �r ett skott�r!");
		
		//Skapar integern 'yearInt' fr�n stringen 'yearString'.
		int yearInt = Integer.parseInt(yearString);
		
		//Skapar en boolean och l�gger den by default till false;
		boolean leapYear = false;
		
		//Om �ret �r delbart med 4
		if(yearInt % 4 == 0)
		{
			//Om �ret �r delbart med 100
			if(yearInt % 100 == 0)
			{
				//Om �ret �r delbart med 400 (�rhundradesiffra delbart med 4)
				if(yearInt % 400 == 0)
				{
					leapYear = true;
				}
				
				//Om �ret �r delbart med 4, 100 och inte 400
				else 
				{
					leapYear = false;
				}
				
			}
			//Om �ret �r delbart med 4 och inte 100
			else 
			{
				
				leapYear = true;
			}
		}
		//Om �ret inte delbart med 4
		else 
		{
			leapYear = false;
		}
		
		//Skriver ut resultatet.
		if(leapYear == true)
		{
			JOptionPane.showMessageDialog(null, yearString + " �r ett skott�r!");
		}
		else
		{
			JOptionPane.showMessageDialog(null, yearString + " �r INTE ett skott�r!");
		}
	}

}
