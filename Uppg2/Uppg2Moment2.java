import java.util.Scanner;

public class Uppg2Moment2 {

	public static void main(String[] args) {
		//
		System.out.println("Think of a number between 1 and 10, and I will guess it! \nTell me if it's higher (h) or lower (h) or if it's correct (c)!");
		
		
		//Genererar en slumpm�ssig integer mellan 10 och 1 som datorns gissning
		int computerGuess = (int)(Math.random() * ((10-1) + 1));

	
		//Skapar en boolean f�r while-loopen nedan
		Boolean correctGuess = false;
		
		//Skapar scannern f�r att kunna ta anv�ndarens input
		Scanner userInput = new Scanner(System.in);
		
		//Medan datorn inte har gissat r�tt tal loopas denna bit
		while(correctGuess != true)
		{
			//skriver ut en gissning
			System.out.println("How about " + computerGuess + "?");
			
			//l�ter anv�ndaren svara
			String userResponse = userInput.nextLine();
	

			if(userResponse.equals("h")  || userResponse.equals("higher"))
			{
				computerGuess += 1;
			}
			if(userResponse.equals("l")  || userResponse.equals("lower"))
			{
				computerGuess -= 1;
			}
			if(userResponse.equals("c")  || userResponse.equals("correct"))
			{
				System.out.println("Yes! I knew you were thinking of the number " + computerGuess + "!");
				correctGuess = true;

			}

		}
	}
}
