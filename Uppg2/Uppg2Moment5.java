import java.util.Arrays;
import java.util.Scanner;

public class Uppg2Moment5 {

	public static void main(String[] args) {
		
		boolean correctNumbers = false;
		//skapar en lottoradsarray med rum f�r 7 element
		int[] lotto = new int[7];
		
		//kallar p� randomLotto-metoden som skapar en slumpm�ssig lottorad
		randomLotto(lotto);
				
		//skapar en lottoradsarray f�r anv�ndaren med rum f�r 7 element
		int [] userLotto = new int[7];
		

		while(correctNumbers!=true)
		{

			
			//kallar p� randomInputLotto-metoden som l�ter anv�ndaren mata in 7 element
			userInputLotto(userLotto);
			
			if(compareLotto(lotto, userLotto))
			{
				correctNumbers = true;
			}
			else
			{
				System.out.println("Try again!");
				Arrays.fill(userLotto, 0);
			}
			
		}
		
		System.out.println("You win!");

	}
	
	
	
	
	
	public static int[] randomLotto(int lottoLine[])
	{

		
		//f�r varje element skapas en random integer
		for(int i = 0; i < lottoLine.length; i++)
		{

			lottoLine[i] = (int)(Math.random() * 39 + 1);
			
			//kollar att det inte kommer n�gra element med samma v�rde
			for(int j = 0; j< i; j++)
			{
				// om lotto[i] �r samma som lotto[j] k�rs yttre loopen igen, genom i--
				if (lottoLine[i] == lottoLine [j])
				{
					i--;
					break;
				}
			}
		}
		
		//sorterar lottoraden i storleksordning
		Arrays.sort(lottoLine);
		
		return lottoLine;
	}
	
	
	
	
	public static int[] userInputLotto(int userLottoLine[])
	{
		//Skapar scannern f�r att kunna ta anv�ndarens input
		Scanner userInput = new Scanner(System.in);
				
		//f�r varje element tas userInput
		for(int i = 0; i < userLottoLine.length; i++)
		{
			
			boolean isInteger = false;
			
			while (isInteger != true) {
				try 
				{
					System.out.println("Enter number #" + (i+1));
					userLottoLine[i] = Integer.parseInt(userInput.nextLine());
				} 
				catch (NumberFormatException e) 
				{
					System.out.println("Input is not a valid integer.");
					isInteger = false;
				}
				if(userLottoLine[i] != 0)
				{
					isInteger = true;
				}

			}

			
				
			//kollar att det inte kommer n�gra element med samma v�rde
			for(int j = 0; j< i; j++)
			{
				// om lotto[i] �r samma som lotto[j] k�rs yttre loopen igen, genom i--
				if (userLottoLine[i] == userLottoLine [j])
				{
					System.out.println("Whoops! You already entered that one!");
					i--;
					break;
				}
				//kollar att anv�ndaren inte matar in v�rden utanf�r intervallet [1,39]
				if (userLottoLine[i] < 1 || userLottoLine[i] > 39)
				{
					System.out.println("Only enter numbers between 1 and 39!");
					i--;
					break;
				}
			}
		}
		//sorterar lottoraden i storleksordning
		Arrays.sort(userLottoLine);
		
		//returnerar anv�ndares lottorad i form av en array
		return userLottoLine;
	}
		
	public static boolean compareLotto(int lottoLine[], int userLottoLine[])
	{
		boolean sameLine = false;
		for(int i = 0; i < lottoLine.length; i++)
		{

			for(int j = 0; j < userLottoLine.length; j++)
			{
				if (userLottoLine[j] == lottoLine[i])
				{
					System.out.println(userLottoLine[j] + " is correct.");
				}

			}
			if(lottoLine[i] != userLottoLine[i])
			{
				sameLine = false;
			}
			else
			{
				sameLine = true;

			}
		}
		return sameLine;
	}
}
