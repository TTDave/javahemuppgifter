import java.util.Scanner;

public class Uppg2Moment8 {
	public static void main(String[] args) {
		
		//Skapar scannern f�r att kunna ta anv�ndarens input
		Scanner userInput = new Scanner(System.in);
		
		//l�ser in anv�ndarens input till h�jd av typen double
		System.out.println("Enter triangle height:");
		double height = userInput.nextDouble();
		
		//l�ser in anv�ndarens input till bas av typen double
		System.out.println("Enter triangle base:");
		double base = userInput.nextDouble();
		
		//kallar hypotenuse metoden och skriver ut v�rdet fr�n en annan klass och l�gger det till en variabel av typ double 'hypotenuse'
		double hypotenuse = Uppg2Moment7.hypotenuse(height, base);
		
		System.out.println("The hypotenuse is " + hypotenuse);
		
	}	
}	




