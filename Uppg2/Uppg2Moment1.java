import java.util.InputMismatchException;
import java.util.Scanner;

public class Uppg2Moment1 {

	public static void main(String[] args) {
		//
		System.out.println("I'm thinking of a number between 1 and 10...");
		
		
		//Genererar en slumpm�ssig integer mellan 10 och 1
		int randomNumber = (int)(Math.random() * ((10-1) + 1));
		
		//Skapar en variabel som h�ller antalet gissningar och nollst�ller den
		int guesses = 0;
	
		//Skapar en boolean f�r while-loopen nedan
		Boolean correctGuess = false;
		
		//Skapar scannern f�r att kunna ta anv�ndarens input
		Scanner userInput = new Scanner(System.in);

	
		//Medan anv�ndaren inte har gissat r�tt tal loopas denna bit
		while(correctGuess != true)
		{
			System.out.println("Try to guess the number!");


			//Loopar tills n�sta v�rde scannern l�ser in �r en integer
			while(!userInput.hasNextInt())
			{
				//L�ser in och kastar bort v�rdet som �r en icke-integer
				userInput.next(); 
				System.out.println("Please enter an integer in numerical format!");
				
			}
			
			//Forts�tter h�r d� anv�ndaren har matat in en giltig variabel
			int userGuess = userInput.nextInt();
			

			if(userGuess > randomNumber)
			{
				guesses += 1;
				System.out.println("The number I'm thinking of is lower than " + userGuess);
			}
			if(userGuess < randomNumber)
			{
				guesses += 1;
				System.out.println("The number I'm thinking of is higher than " + userGuess);	
			}
			if(userGuess == randomNumber)
			{
				guesses += 1;
				System.out.println("Yes! I was thinking of the number " + userGuess);
				correctGuess = true;
			}
				
		}
		System.out.println("You won, and it only took you " + guesses + " guesses!");
		userInput.close();
		
		
		
		

	}

}
