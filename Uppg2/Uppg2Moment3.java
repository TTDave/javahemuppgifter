import java.util.Arrays;
import java.util.Scanner;

public class Uppg2Moment3 {

	public static void main(String[] args) {
		//
		System.out.println("Think of a number between 1 and 10, and I will guess it! \nTell me if it's higher (h) or lower (h) or if it's correct (c)!");
		
		//skapar l�gsta och st�rsta gr�nserna till gissningarna
		int lowRange = 1;
		int highRange = 10;
		
		//skapar variabeln som h�ller datorns gissning
		int computerGuess;

		//Skapar tv� bools f�r while-loopen nedan
		Boolean correctGuess = false;
		
		//Skapar scannern f�r att kunna ta anv�ndarens input
		Scanner userInput = new Scanner(System.in);

		
		//Medan datorn inte har gissat r�tt tal loopas denna bit
		while(correctGuess != true)
		{
			//anv�nder divide and conquer f�r att f�rs�ka hitta r�tt nummer
			computerGuess = (highRange + lowRange)/2;


			//skriver ut gissningen
			System.out.println("How about " + computerGuess + "?");
			
			//l�ter anv�nderen att svara
			String userResponse = userInput.nextLine();
			

			if(userResponse.equals("h")  || userResponse.equals("higher"))
			{
				lowRange = computerGuess + 1;
			}
			if(userResponse.equals("l")  || userResponse.equals("lower"))
			{
				highRange = computerGuess - 1;
			}
			if(userResponse.equals("c")  || userResponse.equals("correct"))
			{
				System.out.println("Yes! I knew you were thinking of the number " + computerGuess + "!");
				correctGuess = true;

			}

		}
	}
}
