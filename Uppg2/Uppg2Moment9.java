import java.util.Scanner;
public class Uppg2Moment9 {

	public static void main(String[] args) {

		//Skapar scannern f�r att kunna ta anv�ndarens input
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number to be converted to binary: ");
		
		//l�gger anv�ndarinput till en integervariabel 'userNumber'
		int userNumber = userInput.nextInt();
		
		//kallar p� convertToBinary-metoden
		convertToBinary(userNumber);
	}
		
	//tar en nummer och konverterar den rekursivt till bin�rt (bas) 
	public static void convertToBinary(int number) {
		if(number > 0)
		{
			convertToBinary(number/2);
			System.out.println(number%2);
		}
	}
	
}
