import java.util.Scanner;

public class Uppg2Moment4 {

	public static void main(String[] args) {
		//
		System.out.println("Think of a number between 1 and 10, and I will guess it! \nTell me if it's higher (h) or lower (h) or if it's correct (c)!");
		
		//skapar l�gsta och st�rsta gr�nserna till gissningarna
		int lowRange = 1;
		int highRange = 10;
		
		//skapar variabeln som h�ller datorns gissning
		int computerGuess;

	
		//Skapar tv� bools f�r while-loopen nedan
		Boolean correctGuess = false;
		Boolean cheater = false;
		
		//Skapar scannern f�r att kunna ta anv�ndarens input
		Scanner userInput = new Scanner(System.in);

		//Skapar en lista som sparar tidigare gissade v�rden f�r att f�rhindra fusk.
		int[] guesses = new int[10];
		int i = 0;
		
		//Medan datorn inte har gissat r�tt tal loopas denna bit
		while(correctGuess != true)
		{
			//anv�nder divide and conquer f�r att f�rs�ka hitta r�tt nummer
			computerGuess = (highRange + lowRange)/2;

			//anti-cheat loop, om gissningen har gjorts tidigare - har anv�ndaren fuskat.
			for(int x : guesses)
			{
				if(x == computerGuess)
				{
					System.out.println("Cheater!");
					cheater = true;
				}
			}
			
			//om anv�ndaren fuskar termineras programmet
			if(cheater == true)
			{
				break;
			}


			//skriver ut gissningen
			System.out.println("How about " + computerGuess + "?");
			
			//l�ter anv�nderen att svara
			String userResponse = userInput.nextLine();
			

			if(userResponse.equals("h")  || userResponse.equals("higher"))
			{
				guesses[i] = computerGuess;
				i++;
				lowRange = computerGuess + 1;
			}
			if(userResponse.equals("l")  || userResponse.equals("lower"))
			{
				guesses[i] = computerGuess;
				i++;
				highRange = computerGuess - 1;
			}
			if(userResponse.equals("c")  || userResponse.equals("correct"))
			{
				System.out.println("Yes! I knew you were thinking of the number " + computerGuess + "!");
				correctGuess = true;

			}

		}
	}
}
