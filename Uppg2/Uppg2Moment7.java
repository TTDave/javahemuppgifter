import java.util.Scanner;

public class Uppg2Moment7 {
	public static void main(String[] args) {
		
		//Skapar scannern f�r att kunna ta anv�ndarens input
		Scanner userInput = new Scanner(System.in);
		
		//l�ser in anv�ndarens input till h�jd av typen double
		System.out.println("Enter triangle height:");
		double height = userInput.nextDouble();
		
		//l�ser in anv�ndarens input till bas av typen double
		System.out.println("Enter triangle base:");
		double base = userInput.nextDouble();
		
		//kallar hypotenuse metoden och skriver ut v�rdet
		System.out.println("The hypotenuse is " + hypotenuse(height, base));
		
	}
	
	
	//tar in tv� doubles, h�jd och bas och returnerar summan av dem i kvadrat
	public static double hypotenuse(double triangleHeight, double triangleBase) {
		return Math.sqrt((triangleHeight * triangleHeight) + (triangleBase + triangleBase)); 		
	}
}




